package ru.tsc.denisturovsky.tm;

import ru.tsc.denisturovsky.tm.constant.TerminalArgument;
import ru.tsc.denisturovsky.tm.constant.TerminalCommand;
import ru.tsc.denisturovsky.tm.model.Command;
import ru.tsc.denisturovsky.tm.util.NumberUtil;

import java.util.Locale;
import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgument(args)) System.exit(0);
        Scanner scanner = new Scanner(System.in);
        showWelcome();
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalArgument.ABOUT:
                showAbout();
                break;
            case TerminalArgument.INFO:
                showSystemInfo();
                break;
            case TerminalArgument.HELP:
                showHelp();
                break;
            case TerminalArgument.VERSION:
                showVersion();
                break;
            default:
                showErrorArgument(arg);
                break;
        }
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalCommand.ABOUT:
                showAbout();
                break;
            case TerminalCommand.INFO:
                showSystemInfo();
                break;
            case TerminalCommand.HELP:
                showHelp();
                break;
            case TerminalCommand.VERSION:
                showVersion();
                break;
            case TerminalCommand.EXIT:
                close();
            default:
                showErrorCommand(command);
                break;
        }
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    public static void close() {
        System.exit(0);
    }

    public static void showErrorCommand(final String command) {
        System.err.format("[Error] \n");
        System.err.format("This command '%s' not supported \n", command);
    }

    public static void showErrorArgument(final String arg) {
        System.err.format("[Error] \n");
        System.err.format("This argument '%s' not supported \n", arg);
    }

    public static void showVersion() {
        System.out.format("[%s] \n", TerminalCommand.VERSION.toUpperCase(Locale.ROOT));
        System.out.format("1.7.0 \n");
    }

    public static void showHelp() {
        System.out.format("[%s] \n", TerminalCommand.HELP.toUpperCase(Locale.ROOT));
        System.out.println(Command.ABOUT);
        System.out.println(Command.INFO);
        System.out.println(Command.HELP);
        System.out.println(Command.VERSION);
        System.out.println(Command.EXIT);
    }

    public static void showAbout() {
        System.out.format("[%s] \n", TerminalCommand.ABOUT.toUpperCase(Locale.ROOT));
        System.out.format("Name: %s %s \n", TerminalCommand.FIRST_NAME, TerminalCommand.LAST_NAME);
        System.out.format("E-mail: %s \n", TerminalCommand.EMAIL);
    }

    public static void showWelcome() {
        System.out.format("%s \n", TerminalCommand.WELCOME);
    }

}

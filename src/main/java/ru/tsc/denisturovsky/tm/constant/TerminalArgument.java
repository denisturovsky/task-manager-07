package ru.tsc.denisturovsky.tm.constant;

public class TerminalArgument {

    public final static String ABOUT = "-a";

    public final static String HELP = "-h";

    public final static String INFO = "-i";

    public final static String VERSION = "-v";

}
